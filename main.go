package main

import (
	"bitbucket.com/trangmaiq/tr_ma/api"
	. "bitbucket.com/trangmaiq/tr_ma/pkg"
	gs "bitbucket.com/trangmaiq/tr_ma/pkg/google"
	"bitbucket.com/trangmaiq/tr_ma/pkg/middleware"
	"bitbucket.com/trangmaiq/tr_ma/pkg/user"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/codegangsta/negroni"
	_ "github.com/go-sql-driver/mysql"
	gctx "github.com/gorilla/context"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {
	db, err := sql.Open("mysql", "root:my-secret-pw@tcp(13.250.96.103:3306)/cyza")
	if err != nil {
		fmt.Println(err)
		return
	}

	dm := user.NewMySQLRepo(db)
	gmailSrv, err := GetGmailSrv()
	if err != nil {
		fmt.Println(err)
		return
	}
	userService := user.NewUserService(dm)
	googleService := gs.NewGoogleService(gmailSrv)
	commonService := NewCommonService(userService, googleService)

	n := negroni.New(
		negroni.HandlerFunc(middleware.Cors),
		negroni.NewLogger(),
	)

	router := mux.NewRouter()
	api.MakeUserHandlers(router, *n, commonService)

	http.Handle("/", router)
	router.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	logger := log.New(os.Stderr, "logger: ", log.Lshortfile)
	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Addr:         ":" + strconv.Itoa(3007),
		Handler:      gctx.ClearHandler(http.DefaultServeMux),
		ErrorLog:     logger,
	}

	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal(err.Error())
	}
}

func GetGmailSrv() (srv *gmail.Service, err error) {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Unable to read client secret file: %v", err))

	}

	config, err := google.ConfigFromJSON(b, gmail.MailGoogleComScope)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Unable to parse client secret file to config: %v", err))
	}
	client := getClient(config)

	srv, err = gmail.New(client)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Unable to retrieve gmail Client %v", err))
	}

	return srv, nil
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
