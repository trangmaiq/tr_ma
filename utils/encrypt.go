package utils

import (
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(pwd string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(pwd), 14)
	return string(bytes), err
}

func CheckPasswordHash(pwd, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pwd))
	return err == nil
}

func GenerateSecretData() (secret, hash string, err error) {
	secret = randSeq(50)
	bytes, err := bcrypt.GenerateFromPassword([]byte(secret), 14)
	hash = string(bytes)
	return secret, hash, err
}
