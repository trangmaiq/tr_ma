package api

import (
	. "bitbucket.com/trangmaiq/tr_ma/pkg"
	"bitbucket.com/trangmaiq/tr_ma/pkg/entity"
	"encoding/json"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func registerHandler(service *Service) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var u entity.User
		if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		u.CreatedAt = time.Now().Unix()
		if u.Password == "" {
			respondWithError(w, http.StatusBadRequest, "password can not be blank")
			return
		}

		if strings.TrimSpace(u.Email) == "" {
			respondWithError(w, http.StatusBadRequest, "email can not be blank")
		}

		userId, err := service.User.Register(&u)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		respondWithJSON(w, http.StatusCreated, userId)
	})
}

func loginHandler(service *Service) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var credentials entity.User
		if err := json.NewDecoder(r.Body).Decode(&credentials); err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		userData, err := service.User.Login(&credentials)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err.Error())
			return
		}

		respondWithJSON(w, http.StatusOK, userData)
	})
}

func updateHandler(service *Service) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		param := mux.Vars(r)
		id, err := strconv.Atoi(param["id"])
		if err != nil {
			respondWithError(w, http.StatusBadRequest, entity.InvalidParams.Error())
		}

		var info entity.User
		if err := json.NewDecoder(r.Body).Decode(&info); err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}
		info.ID = int64(id)

		data, err := service.User.Update(&info)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}
		respondWithJSON(w, http.StatusOK, data)
	})
}

func forgotPasswordHandler(service *Service) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var info entity.User
		if err := json.NewDecoder(r.Body).Decode(&info); err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		if strings.TrimSpace(info.Email) == "" {
			respondWithError(w, http.StatusBadRequest, entity.InvalidParams.Error())

		}

		token, err := service.User.ForgotPassword(info.Email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		message := `Hello!
To reset your password, click the link: localhost/user/reset_password?reset_token=` + token

		err = service.Google.SendGmail(info.Email, message)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
		}

		respondWithJSON(w, http.StatusOK, "your reset password link has been sent to your email")
	})
}

func MakeUserHandlers(r *mux.Router, n negroni.Negroni, service *Service) {
	r.Handle("/register", n.With(
		negroni.Wrap(registerHandler(service)),
	)).Methods("POST", "OPTIONS").Name("register")
	r.Handle("/login", n.With(
		negroni.Wrap(loginHandler(service)),
	)).Methods("POST", "OPTIONS").Name("login")
	r.Handle("/users/{id}", n.With(
		negroni.Wrap(updateHandler(service)),
	)).Methods("PUT", "OPTIONS").Name("update")
	r.Handle("/users/forgot_password", n.With(
		negroni.Wrap(forgotPasswordHandler(service)),
	)).Methods("POST", "OPTIONS").Name("forgot_password")
}
