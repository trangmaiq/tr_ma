package pkg

import (
	"bitbucket.com/trangmaiq/tr_ma/pkg/google"
	"bitbucket.com/trangmaiq/tr_ma/pkg/user"
)

type Service struct {
	User   user.Service
	Google google.Service
}

func NewCommonService(us user.Service, gs google.Service) *Service {
	return &Service{
		User:   us,
		Google: gs,
	}
}
