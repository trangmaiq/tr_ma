package google

import (
	"encoding/base64"
	"errors"
	"fmt"
	"google.golang.org/api/gmail/v1"
	"strings"
)

type Service interface {
	SendGmail(to string, message string) error
}

type googleService struct {
	service *gmail.Service
}

func NewGoogleService(s *gmail.Service) Service {
	return &googleService{
		service: s,
	}
}

func (gms *googleService) SendGmail(to string, msg string) error {
	var message gmail.Message
	temp := []byte("From: 'Account Service'\r\n" +
		"To:  " + to + "\r\n" +
		"Subject: " + "Request to reset your account password" + "\r\n" +
		"\r\n" + msg)

	message.Raw = base64.StdEncoding.EncodeToString(temp)
	message.Raw = strings.Replace(message.Raw, "/", "_", -1)
	message.Raw = strings.Replace(message.Raw, "+", "-", -1)
	message.Raw = strings.Replace(message.Raw, "=", "", -1)

	_, err := gms.service.Users.Messages.Send("me", &message).Do()
	if err != nil {
		return errors.New(fmt.Sprintf("Unable to send. %v", err))
	}

	return nil
}
