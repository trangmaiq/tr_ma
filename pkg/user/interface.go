package user

import "bitbucket.com/trangmaiq/tr_ma/pkg/entity"

type Reader interface {
	FindById(id int64) (*entity.User, error)
	FindByEmail(email string) (*entity.User, error)
}

type Writer interface {
	Store(user *entity.User) (int64, error)
	Update(user *entity.User) error
	//Delete(id int64) error
}

type Repository interface {
	Reader
	Writer
}
