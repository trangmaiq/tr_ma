package user

import (
	"bitbucket.com/trangmaiq/tr_ma/pkg/entity"
	"database/sql"
	"fmt"
	"log"
)

type repository struct {
	db *sql.DB
}

func NewMySQLRepo(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) FindById(id int64) (*entity.User, error) {
	var u entity.User
	statement := fmt.Sprintf("SELECT * FROM user WHERE user_id = %d", id)
	log.Println(statement)
	err := r.db.QueryRow(statement).Scan(&u.ID, &u.Email, &u.Password, &u.CreatedAt, &u.Address, &u.Telephone, &u.FullName, &u.ResetToken)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println(statement)
			log.Println(err)
			return nil, err
		}
		return nil, entity.ErrNotFound
	}
	return &u, nil
}

func (r *repository) FindByEmail(email string) (*entity.User, error) {
	var u entity.User
	statement := fmt.Sprintf("SELECT * FROM user WHERE email = '%s' LIMIT 1", email)
	err := r.db.QueryRow(statement).Scan(&u.ID, &u.Email, &u.Password, &u.CreatedAt, &u.Address, &u.Telephone, &u.FullName, &u.ResetToken)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println(statement)
			log.Println(err)
			return nil, err
		}
		return nil, entity.ErrNotFound
	}

	return &u, nil
}

func (r *repository) Store(user *entity.User) (int64, error) {
	statement := fmt.Sprintf("INSERT INTO user(email, password, address, telephone, created_at, full_name) VALUES ('%s', '%s', '%s', '%s', %d, '%s')",
		user.Email, user.Password, user.Address, user.Telephone, user.CreatedAt, user.FullName)

	result, err := r.db.Exec(statement)
	if err != nil {
		log.Println(statement)
		return 0, err
	}

	id, _ := result.LastInsertId()
	return id, nil
}

func (r *repository) Update(data *entity.User) error {
	statement := fmt.Sprintf("UPDATE user SET email='%s',password='%s',address='%s',telephone='%s',full_name='%s',reset_token='%s'  WHERE user_id=%d",
		data.Email, data.Password, data.Address, data.Telephone, data.FullName, data.ResetToken, data.ID)

	_, err := r.db.Exec(statement)

	if err != nil {
		return err
	}

	return err
}
