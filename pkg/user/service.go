package user

import (
	"bitbucket.com/trangmaiq/tr_ma/pkg/entity"
	"bitbucket.com/trangmaiq/tr_ma/utils"
	"database/sql"
	"errors"
)

type Service interface {
	Register(user *entity.User) (*entity.User, error)
	Login(user *entity.User) (*entity.User, error)
	ForgotPassword(email string) (token string, err error)
	ChangePassword(user *entity.User, password string) error
	Update(data *entity.User) (*entity.User, error)
	GetRepository() Repository
}

type userService struct {
	repository Repository
}

func NewUserService(r Repository) Service {
	return &userService{
		repository: r,
	}
}

func (us *userService) GetRepository() Repository {
	return us.repository
}

func (us *userService) Register(user *entity.User) (*entity.User, error) {
	u, err := us.repository.FindByEmail(user.Email)
	if u != nil {
		return nil, errors.New("this email already exists")
	}

	pwdHash, err := utils.HashPassword(user.Password)
	if err != nil {
		return nil, err
	}
	user.Password = pwdHash

	userId, err := us.repository.Store(user)
	if err != nil {
		return nil, err
	}

	user.ID = userId
	user.Password = ""
	return user, nil
}

func (us *userService) Login(data *entity.User) (*entity.User, error) {
	user, err := us.repository.FindByEmail(data.Email)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
		return nil, errors.New("email or password is incorrect")
	}

	if isCorrect := utils.CheckPasswordHash(data.Password, user.Password); !isCorrect {
		return nil, errors.New("email or password is incorrect")
	}

	user.Password = ""
	return user, nil
}

func (us *userService) Update(data *entity.User) (*entity.User, error) {
	user, err := us.repository.FindById(data.ID)
	if err != nil {
		if err != entity.ErrNotFound {
			return nil, err
		}
		return nil, errors.New("user_id not found")
	}

	if data.FullName != "" {
		user.FullName = data.FullName
	}
	if data.Telephone != "" {
		user.Telephone = data.Telephone
	}
	if data.Address != "" {
		user.Address = data.Address
	}
	if data.Email != "" {
		user.Email = data.Email
	}

	if err := us.repository.Update(user); err != nil {
		return nil, err
	}

	user.Password = ""
	return user, nil
}

func (us *userService) ForgotPassword(email string) (string, error) {
	user, err := us.repository.FindByEmail(email)
	if err != nil {
		if err != sql.ErrNoRows {
			return "", err
		}
		return "", errors.New("email does not exist")
	}

	secret, hash, err := utils.GenerateSecretData()
	user.ResetToken = hash
	if err := us.repository.Update(user); err != nil {
		return "", err
	}
	return secret, nil
}

func (us *userService) ChangePassword(user *entity.User, password string) error {
	return nil
}
