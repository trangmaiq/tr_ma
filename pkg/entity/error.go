package entity

import "errors"

var (
	ErrNotFound   = errors.New("not found")
	InvalidParams = errors.New("invalid params")
)
