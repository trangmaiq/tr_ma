package entity

type User struct {
	ID         int64  `json:"user_id"`
	Email      string `json:"email"`
	Password   string `json:"password,omitempty"`
	FullName   string `json:"full_name"`
	Address    string `json:"address"`
	Telephone  string `json:"telephone"`
	CreatedAt  int64  `json:"created_at"`
	ResetToken string `json:"reset_token,omitempty"`
}
